class TCP_Server
  require 'socket'
  require_relative 'pool'
  kill = false
  p = Pool.new(10)
  server = TCPServer.new 4242
  until kill do
    client = server.accept
    p.schedule do
      puts "New connection from #{client.peeraddr[2]}"
      input = client.gets
      puts "Input received:"
      if input != nil
        input = input.chomp
      end
      puts input
      if input == "KILL_SERVICE"
        puts "Killing server"
        client.puts "You killed me!"
        exit(0)
      elsif input[0, 5] == "HELO "
        sock_domain, remote_port, remote_hostname, remote_ip = client.peeraddr
        client.puts "#{input}\nIP:#{remote_ip}\nPort:#{remote_port}\nStudentID:11387266"
      else
        client.puts "Received #{input}"
      end
      puts "Sleep to delay thread disconnect"
      sleep(15)
      puts 'Close'
      client.close
    end
  end
end
