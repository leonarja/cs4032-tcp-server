class TCP_Client
  require 'socket'      # Sockets are in standard library

  hostname = 'localhost'
  port = 4242

  s = TCPSocket.open(hostname, port)

  puts "Awaiting input"
  s.write(gets)
  puts "Input received and sent"
  while line = s.gets   # Read lines from the socket
    puts line.chop      # And print with platform line terminator
  end
  puts "Closing"
  s.close               # Close the socket when done
end
